package org.jlu.dede.gateway.gatewayfeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "service-information",url = "10.100.0.5:8769")
public interface GateWayFeign {
    @RequestMapping(value = "/passenger/verification", method=RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> verification(@RequestParam("token") String token);
}