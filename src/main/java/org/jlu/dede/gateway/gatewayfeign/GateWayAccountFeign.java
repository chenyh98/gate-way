package org.jlu.dede.gateway.gatewayfeign;


import org.jlu.dede.publicUtlis.model.*;
import org.jlu.dede.publicUtlis.model.Account;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "data-access",url = "10.100.0.2:8765")
public interface GateWayAccountFeign {
    @RequestMapping(value = "/accounts/{id}", method= RequestMethod.GET)
    Account getByID(@PathVariable Integer id);

}
