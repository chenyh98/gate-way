package org.jlu.dede.gateway.Json;

import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.http.HttpStatus;


public class AuthFailException extends RestException {
    public AuthFailException(String msg) {
        super(706, msg);
    }


}
