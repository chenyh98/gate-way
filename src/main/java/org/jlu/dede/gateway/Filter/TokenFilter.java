package org.jlu.dede.gateway.Filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.jlu.dede.gateway.Json.AuthFailException;
import org.jlu.dede.gateway.gatewayfeign.GateWayAccountFeign;
import org.jlu.dede.gateway.gatewayfeign.GateWayFeign;
import org.jlu.dede.publicUtlis.json.RestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

@Component
public class TokenFilter extends ZuulFilter {
    @Autowired
    GateWayFeign gateWayFeign;
    @Autowired
    GateWayAccountFeign gateWayAccountFeign;
    @Override
    public String filterType(){//路由映射的阶段是寻找路由映射表
        return  "pre";
    }//前置拦截，请求被路由之前调用
    @Override
    public int filterOrder(){//自定义过滤器执行顺序，数值越大越往后执行
        return 0;
    }//过滤器执行顺序，越小越先执行
    @Override
    public boolean shouldFilter(){//控制过滤器是否生效

        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest  request = requestContext.getRequest();//代表客户端请求，封装http请求头中所有信息
        if("/driver-api/driver/register/send".equalsIgnoreCase(request.getRequestURI())){
            return false;//这里缺少注册的一部分
        }
        else if("/driver-api/driver/register/verify".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }
        else if("/driver-api/driver/logIn".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }
        else if("/driver-api/driver/logOut".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }
        else if("/passenger-api/passenger/register/send".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }
        else if("/passenger-api/passenger/register/verify".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }
        else if("/passenger-api/passenger/logIn".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }
        else if("/passenger-api/passenger/logOut".equalsIgnoreCase(request.getRequestURI())){
            return false;
        }

        return true;
    }
   @Override
    public Object run()throws ZuulException {//提示不需要throws ZuulException ？？？j
        //requestContext()详解？？？存储http请求
        //登录校验逻辑
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();//获得当前请求的对象
        String token = request.getHeader("token");
        System.out.println("token!!!!!!!!"+token);
        Map<String, Object> result ;
        try {
            result = gateWayFeign.verification(token).getBody();//这里为什么要加一个getBody，Body是什么
        } catch (RestException e) {//应该自己实现异常
            //相关操作
            if(e.getErrorCode()-703==0||e.getErrorCode()-704 ==0){
                requestContext.setResponseStatusCode(401);//响应状态码
                System.out.println("ce测试IF"+token);
            }
            else
                requestContext.setResponseStatusCode(e.getStatus().value());//响应状态码
            requestContext.setSendZuulResponse(false);//终止转发，返回响应报文
            requestContext.setResponseBody(e.getMessage());//错误报文
            return null;//提前结束
        }
        String user_id=(String) result.get("user_id");
        String account_id=(String) result.get("account_id") ;
        String refreshtoken=(String) result.get("token");
        if(refreshtoken!=null){
            HttpServletResponse response = requestContext.getResponse();
            response.setHeader("token", refreshtoken);
            requestContext.setResponse(response);
        }
        int type=gateWayAccountFeign.getByID(Integer.parseInt(account_id)).getType();
        String judge=null;
        if(type==1){
            //requestContext.addZuulRequestHeader("type","passenger");
            judge="passenger";
        }
        else
            judge="driver";
        String url=request.getRequestURI();
        if(!url.toLowerCase().contains(judge)){
            AuthFailException e=new AuthFailException("Illegal requests");
            requestContext.setSendZuulResponse(false);
            requestContext.setResponseStatusCode(e.getErrorCode());//需要修改错误码
            requestContext.setResponseBody("Illegal requests");
            throw e;

        }
        // requestContext.addZuulRequestHeader("type","driver");
        requestContext.addZuulRequestHeader("user_id",user_id);
        requestContext.addZuulRequestHeader("account_id",account_id);
        return null;




    }
}
